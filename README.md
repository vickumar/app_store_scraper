App Store Scraper
=================

Install Python
---------------

  * Download https://www.python.org/ftp/python/2.7.9/python-2.7.9.msi 
  * Run the downloaded file 
  * Follow the default options
  * Python will install to C:\Python27
  
Using with 64-bit Python
------------------------

  * Open a terminal and type "pip uninstall lxml"
  * Download and install the 64-bit lxml package here: https://pypi.python.org/packages/2.7/l/lxml/
  * In the app scraper directory open the 'scrape.bat' file and remove the first line:  "pip install -r requirements.txt"

Configure Python
----------------
    
  * Click the start button
  * Right click on the My Computer
  * Select properties
  * Select Advanced System Settings
  * click on "environment variables"
  * in the system variables box, find the variable "path" and click edit
  * in the "Variable value" box, at the **end** of the entry, add the following text
  * `;C:\Python27;C:\Python27\Scripts`
  * click ok through all of the menus
  
Install Pip 
-----------

  * https://pip.pypa.io/en/stable/installing/

Install Requirements
--------------------  
  * Press the `windows logo key` and `r` at the same time
  * Enter `cmd` and press `ENTER`
  * Navigate to the app store scraper directory
  * Type `pip install -r requirements.txt`, and press `ENTER`

Running App Store Scraper
-------------------------

  * Navigate to root directory of the app store scraper project
  * Double-click on `run.bat`
  * A command prompt will open 
  * The program will print messages to keep track of  progress and will prompt you when it is finished
  * The program can take up to six hours to run
  
Adding and removing Apps
------------------------

  * Navigate to the config directory
  * Open the `config.json` file
  * Modify the json file with your new app and app id
 
Finding IDs of Apps
-------------------

  * for itunes, find the website of the app (via google, usually). i.e. for marriott, the website is `https://itunes.apple.com/us/app/marriott-international/id455004730?mt=8`
  * the numbers between id and ?mt=8 are the id
  * for the play store, find the website of the app (for marriott, the website is `https://play.google.com/store/apps/details?id=com.marriott.mrt`)
  * the text following id= is the id
  * the program will perform error checking of the ids, in case any ids are entered incorrectly
