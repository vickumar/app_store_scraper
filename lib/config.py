import json, os, requests


class Config(object):
    @staticmethod
    def load_config(file_path):
        with open(file_path, "r") as config_file:
            config_settings = json.load(config_file)
        return config_settings

    @staticmethod
    def has_internet_connection():
        try:
            requests.get('https://www.google.com')
        except requests.ConnectionError:
            print('No connection detected!\n')
            print('Please check your connection and try again.\n')
            return False
        return True

    @staticmethod
    def summarize_review(summary_file, file_path, output_dir=""):
        # create dictionary to store summary in
        summ_dict = dict()

        with open(os.path.join(output_dir, file_path), 'r') as f:
            for i, line in enumerate(f):
                line_list = line.split('|')
                key = '|'.join(line_list[:-1])

                if i:
                    if key in summ_dict:
                        summ_dict[key] += 1

                    else:
                        summ_dict[key] = 1

                else:
                    header = key + '|count\n'

        with open(os.path.join(output_dir, summary_file), 'w') as f:
            # write header to file
            f.write(header)

            # write summarized reviews to file
            for review in summ_dict:
                f.write('%s|%d\n' % (review, summ_dict[review]))
