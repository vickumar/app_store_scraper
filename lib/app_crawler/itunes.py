import os
from app_crawler import AppCrawler, AppReview
from lxml import etree


class ITunesCrawler(AppCrawler):
    _base_url = "https://itunes.apple.com/us/rss/customerreviews/page=%s/id=%s/sortBy=mostRecent/xml"

    def __new__(cls, *args, **kwargs):
        return super(ITunesCrawler, cls).__new__(cls, *args, **kwargs)

    def _base_app_url(self, app_id, page_num):
        return self._base_url % (page_num, app_id)

    @staticmethod
    def parse_app_reviews(review_data):
        app_reviews = list()
        root = etree.fromstring(review_data)

        for child in root:
            if child.tag[-5:] == 'entry':
                updated = None
                rank = None
                review = None
                version = None
                for el in child:
                    if el.tag[-7:] == 'content' and el.text.strip()[1:6] != 'table':
                        review = el.text.strip()
                    elif el.tag[-7:] == 'updated':
                        updated = el.text[0:10]
                    elif el.tag[-7:] == 'version':
                        version = "'%s'" % el.text.strip()
                    elif el.tag[-6:] == 'rating':
                        rank = el.text.strip()

                if updated and rank and review and version:
                    app_reviews.append(AppReview(updated, rank, review, version))
        return app_reviews

    def find_app_reviews(self, app_id, page_num=1):
        try:
            review_data = self._session.get(url=self._base_app_url(app_id, page_num))
            reviews = ITunesCrawler.parse_app_reviews(review_data.content)
            if page_num < 10:
                reviews.extend(self.find_app_reviews(app_id, page_num + 1))
            return reviews
        except etree.XMLSyntaxError:
            print("error reading app id: %s, page: %s" % (app_id, page_num))
            return list()

    @staticmethod
    def _load_existing_reviews(file_name, output_dir=""):
        print("Finding existing reviews...")
        reviews = dict()
        target = open(os.path.join(output_dir, file_name), 'r')
        for r in target.readlines():
            (_app_name, _version, _updated, _rank, _review) = tuple(r.split('|'))
            unique_key = AppCrawler.mk_unique_key(_app_name, _updated, _review)
            reviews[unique_key] = r
        target.close()
        return reviews

    def add_reviews_to_file(self, file_name, apps_list, output_dir=""):
        reviews_added = 0
        old_reviews = ITunesCrawler._load_existing_reviews(file_name, output_dir)
        print("Found %s existing iTunes reviews...\n" % len(old_reviews))
        print("Downloading iTunes reviews...\n")
        target = open(os.path.join(output_dir, file_name), 'a')

        for app_name, app_id in apps_list.items():
            print("Downloading %s..." % app_name)
            reviews = self.find_app_reviews(app_id)
            for review in reviews:
                try:
                    unique_key = AppCrawler.mk_unique_key(app_name, review._updated, review._review)

                    if old_reviews.get(unique_key) is None:
                        target.write(
                            "%s|%s|%s|%s|%s\n" % (app_name,
                                                  review._version,
                                                  review._updated,
                                                  review._rank,
                                                  review._review)
                        )
                        reviews_added += 1
                except:
                    pass
        target.close()
        return reviews_added


