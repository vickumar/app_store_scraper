import os, urllib2
from app_crawler import AppCrawler, AppReview, GoogleAppParser
from BeautifulSoup import BeautifulSoup


class GooglePlayCrawler(AppCrawler):
    _base_url = "https://play.google.com/store/apps/details?id=%s"

    def __new__(cls, *args, **kwargs):
        return super(GooglePlayCrawler, cls).__new__(cls, *args, **kwargs)

    def _base_app_url(self, app_id):
        return self._base_url % app_id

    @staticmethod
    def parse_app_reviews(review_data):
        app_reviews = list()
        soup = BeautifulSoup(review_data)
        reviews = soup.findAll('div', {'class': 'single-review'})

        for r in reviews:
            try:
                review_text = GoogleAppParser.parse_review_text(
                    r.findAll('div', {'class': 'review-body with-review-wrapper'})[0])
                review_date = GoogleAppParser.parse_review_date(r.findAll('span', {'class': 'review-date'})[0])
                review_rating = GoogleAppParser.parse_review_rating(
                    r.findAll('div', {'class': 'review-info-star-rating'})[0])

                app_reviews.append(AppReview(review_date, review_rating, review_text))
            except Exception as e:
                print(e)

        return app_reviews

    def find_app_reviews(self, app_id):
        try:
            user_agent = 'Mozilla/5 (Solaris 10) Gecko'
            headers = { 'User-Agent': user_agent }
            request = urllib2.Request(url=self._base_app_url(app_id), headers=headers)
            response = urllib2.urlopen(request)
            review_data = response.read()
            response.close()

            return GooglePlayCrawler.parse_app_reviews(review_data)
        except Exception as e:
            print("error reading app id: %s" % app_id)
            print(e)
            return list()

    @staticmethod
    def _load_existing_reviews(file_name, output_dir=""):
        print("Finding existing reviews...")
        reviews = dict()
        target = open(os.path.join(output_dir, file_name), 'r')
        for r in target.readlines():
            (_app_name, _updated, _rank, _review) = tuple(r.split('|'))
            unique_key = AppCrawler.mk_unique_key(_app_name, _updated, _review)
            reviews[unique_key] = r
        target.close()
        return reviews

    def add_reviews_to_file(self, file_name, apps_list, output_dir=""):
        reviews_added = 0
        old_reviews = GooglePlayCrawler._load_existing_reviews(file_name, output_dir)
        print("Found %s existing Google Play reviews...\n" % len(old_reviews))
        print("\nDownloading Google Play reviews...\n")
        target = open(os.path.join(output_dir, file_name), 'a')

        for app_name, app_id in apps_list.items():
            print("Downloading %s..." % app_name)
            reviews = self.find_app_reviews(app_id)
            for review in reviews:
                try:
                    unique_key = AppCrawler.mk_unique_key(app_name, review._updated, review._review)
                    if old_reviews.get(unique_key) is None:
                        target.write(
                            "%s|%s|%s|%s\n" % (app_name,
                                               review._updated,
                                               review._rank,
                                               review._review)
                        )
                        reviews_added += 1
                except:
                    pass
        target.close()
        return reviews_added
