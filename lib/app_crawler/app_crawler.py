import datetime, hashlib, requests


class AppReview(object):
    _updated = None
    _rank = None
    _review = None
    _version = None

    def __init__(self, updated, rank, review, version=None):
        self._updated = updated
        self._rank = rank
        self._review = review.replace('\n', ' ').replace('\r', '').strip()
        self._version = version


class AppCrawler(object):
    _session = requests.Session()

    def __init__(self):
        self._session.mount("http://", requests.adapters.HTTPAdapter(max_retries=5))
        self._session.mount("https://", requests.adapters.HTTPAdapter(max_retries=5))

    @staticmethod
    def mk_unique_key(app_name, updated, review):
        return hashlib.md5("%s|%s|%s" % (app_name, updated, review.strip()[0:16])).hexdigest()

    def find_app_reviews(self, app_id):
        raise NotImplementedError

    def add_reviews_to_file(self, file_name, app_list):
        raise NotImplementedError


class GoogleAppParser(object):
    @staticmethod
    def parse_review_text(review):
        if review:
            review = str(review)
            review = review.split("</span>")[1]
            review = review.split("<div ")[0]
            review = review.strip()
        return review

    @staticmethod
    def parse_review_rating(rating):
        if rating:
            rating = str(rating)
            rating = rating.split("Rated")[1]
            rating = rating.split("stars")[0]
            rating = rating.strip()
        return rating

    @staticmethod
    def parse_review_date(review_date):
        if review_date:
            review_date = str(review_date)
            review_date = review_date.split(">")[1]
            review_date = review_date.split("<")[0]
            review_date = review_date.strip()
            review_date = datetime.datetime.strptime(review_date, '%B %d, %Y')
            review_date = review_date.strftime('%Y-%m-%d')
        return review_date
