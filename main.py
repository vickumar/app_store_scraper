import os, sys
from time import time
from lib.config import Config
from lib.app_crawler.google_play import GooglePlayCrawler
from lib.app_crawler.itunes import ITunesCrawler


def _cleanup_output_directory(output_path, config=None):
    try:
        for f in os.listdir(output_path):
            if f != config['output']['itunes.reviews'] and f != config['output']['google_play.reviews']:
                os.remove(os.path.join(output_path, f))
    except:
        os.makedirs(output_path)


def run_crawler():
    _config = Config.load_config("config/config.json")
    print("\nLoaded configuration\n")

    start = time()
    output_path = _config['output']['directory']
    _cleanup_output_directory(output_path, _config)
    itunes, play = ITunesCrawler(), GooglePlayCrawler()

    itunes_reviews = itunes.add_reviews_to_file(_config['output']['itunes.reviews'],
                                                _config['sources']['itunes'], output_path)
    print("\nAdded %s new reviews...\n" % itunes_reviews)

    play_reviews = play.add_reviews_to_file(_config['output']['google_play.reviews'],
                                            _config['sources']['google_play'], output_path)
    print("\nAdded %s new reviews...\n" % play_reviews)

    print("\nSummarizing iTunes reviews...\n")
    Config.summarize_review(_config['output']['itunes.summary'],
                            _config['output']['itunes.reviews'], output_path)

    print("\nSummarizing Google Play reviews...\n")
    Config.summarize_review(_config['output']['google_play.summary'],
                            _config['output']['google_play.reviews'], output_path)

    elapsed = time() - start
    return elapsed / 3600, (elapsed / 60) % 60, elapsed % 60


if __name__ == '__main__':
    if not Config.has_internet_connection():
        done = raw_input('DONE. Press ENTER to continue\n>>> ')
        exit(1)

    hours, minutes, seconds = run_crawler()
    print('DONE in %02d:%02d:%02.3f' % (hours, minutes, seconds))
    done = raw_input('Press ENTER to continue\n>>> ')
    sys.exit(0)
